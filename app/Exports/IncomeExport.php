<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class IncomeExport implements FromView, ShouldAutoSize
{
    private $dates;
    private $tgl_awal;
    private $tgl_akhir;
    private $market;
    public function __construct($dates, $tgl_awal, $tgl_akhir, $market)
    {
        $this->dates = $dates;
        $this->tgl_awal = $tgl_awal;
        $this->tgl_akhir = $tgl_akhir;
        $this->market = $market;
    }
    public function view(): View
    {
        return view('report.export_report_income_excel', [
            'dates' => $this->dates,
            'tgl_awal' => $this->tgl_awal,
            'tgl_akhir' => $this->tgl_akhir,
            'market' => $this->market
        ]);
    }
}
