<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class StockExport implements FromView, ShouldAutoSize
{
    private $products;
    private $supply_system;
    private $count;
    public function __construct($products, $supply_system, $count)
    {
        $this->products = $products;
        $this->supply_system = $supply_system;
        $this->count = $count;
    }
    public function view(): View
    {
        return view('report.export_report_stock_excel', [
            'products' => $this->products,
            'supply_system' => $this->supply_system,
            'count' => $this->count
        ]);
    }
}
