<?php

namespace App\Http\Controllers;

use PDF;
use Auth;
use Session;
use App\Acces;
use App\Market;
use App\Product;
use App\Distributor;
use App\Activity;
use App\Transaction;
use App\Supply_system;
use App\Supply;
use Illuminate\Http\Request;

class TransactionManageController extends Controller
{
    // Show View Transaction
    public function viewTransaction()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
        	$products = Product::all()
        	->sortBy('kode_barang');
            $distributors = Distributor::all()
        	->sortBy('nama');
            $supply_system = Supply_system::first();

            return view('transaction.transaction', compact('products', 'supply_system', 'distributors'));
        }else{
            return back();
        }
    }

    // Refresh Stock
    public function refreshStock()
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
        	$products = Product::all()
        	->sortBy('kode_barang');
            $distributors = Distributor::all();
            return response()->json([
        		'products' => $products,
                'distributors' => $distributors
        	]);
        }else{
            return back();
        }
    }

    // Take Transaction Product
    public function transactionProduct($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
        	$product = Product::where('kode_barang', '=', $id)
        	->first();
        	$supply_system = Supply_system::first();
        	$status = $supply_system->status;

        	return response()->json([
        		'product' => $product,
        		'status' => $status
        	]);
        }else{
            return back();
        }
    }

    // Check Transaction Product
    public function transactionProductCheck($id)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
        	$product_check = Product::where('kode_barang', '=', $id)
        	->count();

        	if($product_check != 0){
        		$product = Product::where('kode_barang', '=', $id)
    	    	->first();
    	    	$supply_system = Supply_system::first();
    	    	$status = $supply_system->status;
        		$check = "tersedia";
        	}else{
        		$product = '';
        		$status = '';
        		$check = "tidak tersedia";
        	}

        	return response()->json([
        		'product' => $product,
        		'status' => $status,
        		'check' => $check
        	]);
        }else{
            return back();
        }
    }

    // Transaction Process
    public function transactionProcess(Request $req)
    {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
    		$jml_barang = count($req->kode_barang);
        	for($i = 0; $i < $jml_barang; $i++){
        		$transaction = new Transaction;
        		$transaction->kode_transaksi = $req->kode_transaksi;
        		$transaction->kode_barang = $req->kode_barang[$i];
                $product_data = Product::where('kode_barang', $req->kode_barang[$i])
                ->first();
                $transaction->nama_barang = $product_data->nama_barang;
                $transaction->hpp = $product_data->hpp;
                if($req->price_status === '1') {
                    $transaction->harga = $product_data->harga;
                } else {
                    $transaction->harga = $product_data->harga_reseller;
                }
        		$transaction->jumlah = $req->jumlah_barang[$i];
        		$transaction->total_barang = $req->total_barang[$i];
                $transaction->total_barang_hpp = $req->total_barang_hpp[$i];
        		$transaction->subtotal = $req->subtotal;
                $transaction->subtotal_hpp = $req->subtotal_hpp;
        		$transaction->diskon = $req->diskon;
        		$transaction->total = $req->total;
                $transaction->total_hpp = $req->total_hpp;
        		$transaction->bayar = $req->bayar;
                if($req->pelanggan) {
                    $transaction->pelanggan = $req->pelanggan;
                    $transaction->id_pelanggan = $req->id_pelanggan;
                } else {
                    $transaction->pelanggan = 'No Name';
                    $transaction->id_pelanggan = 0;
                }
                $transaction->po = $req->po;
                if($req->name_add_value) {
                    $transaction->add_val = $req->name_add_value;
                    $transaction->value_add_val = $req->value_add_value;
                } else {
                    $transaction->add_val = null;
                    $transaction->value_add_val = 0;
                }
        		$transaction->kembali = $req->bayar - $req->total;
        		$transaction->id_kasir = Auth::id();
                $transaction->kasir = Auth::user()->nama;
        		$transaction->save();
        	}

        	$check_supply_system = Supply_system::first();
        	if($check_supply_system->status == true){
        		for($j = 0; $j < $jml_barang; $j++){
        			$product = Product::where('kode_barang', '=', $req->kode_barang[$j])
        			->first();
        			$product->stok = $product->stok - $req->jumlah_barang[$j];
        			$product->save();
                    $product_status = Product::where('kode_barang', '=', $req->kode_barang[$j])
                    ->first();
                    if($product_status->stok == 0){
                        $product_status->keterangan = 'Habis';
                        $product_status->save();
                    }
        		}
        	}

            $activity = new Activity;
            $activity->id_user = Auth::id();
            $activity->user = Auth::user()->nama;
            $activity->nama_kegiatan = 'transaksi';
            $activity->jumlah = $jml_barang;
            $activity->save();

        	Session::flash('transaction_success', $req->kode_transaksi);

        	return back();
        }else{
            return back();
        }
    }

    // Transaction Receipt
    public function receiptTransaction($id)
    {
        $market = Market::first();
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
            $transaction = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->first();
            $transactions = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->get();
            $diskon = $transaction->subtotal * $transaction->diskon / 100;

            $customPaper = array(0,0,400.00,283.80);
            $pdf = PDF::loadview('transaction.receipt_transaction', compact('transaction', 'transactions', 'diskon', 'market'))->setPaper($customPaper, 'landscape');
            return $pdf->stream();
        }else{
            return back();
        }
    }

    // Transaction Receipt
    public function receiptTransactionFull($id)
    {
        $market = Market::first();
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
            $transaction = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->first();
            $transactions = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->get();
            $diskon = $transaction->subtotal * $transaction->diskon / 100;
            // return view('transaction.receipt_transaction_invoice', compact('transaction', 'transactions', 'diskon', 'market'));
            $pdf = PDF::loadview('transaction.receipt_transaction_invoice', compact('transaction', 'transactions', 'diskon', 'market'));
            return $pdf->stream();
        }else{
            return back();
        }
    }

    public function receiptTransactionDo($id)
    {
        $market = Market::first();
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->transaksi == 1){
            $transaction = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->first();
            $transactions = Transaction::where('transactions.kode_transaksi', '=', $id)
            ->select('transactions.*')
            ->get();
            $diskon = $transaction->subtotal * $transaction->diskon / 100;
            // return view('transaction.receipt_transaction_do', compact('transaction', 'transactions', 'diskon', 'market'));
            $pdf = PDF::loadview('transaction.receipt_transaction_do', compact('transaction', 'transactions', 'diskon', 'market'));
            return $pdf->stream();
        }else{
            return back();
        }
    }

    public function changepocashTransaction($id) {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $transaction = Transaction::where('kode_transaksi', $id)->get();
            $jml_barang = count($transaction);
        	for($i = 0; $i < $jml_barang; $i++){
        		$transaction[$i]->po = 2;
        		$transaction[$i]->save();
        	}

            Session::flash('update_success', 'Data PO berhasil diubah');

            return back();
        }else{
            return back();
        }
    }

    public function changepotransferTransaction($id) {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $transaction = Transaction::where('kode_transaksi', $id)->get();
            $jml_barang = count($transaction);
        	for($i = 0; $i < $jml_barang; $i++){
        		$transaction[$i]->po = 3;
        		$transaction[$i]->save();
        	}

            Session::flash('update_success', 'Data PO berhasil diubah');

            return back();
        }else{
            return back();
        }
    }

    public function returTransaction($id) {
        $id_account = Auth::id();
        $check_access = Acces::where('user', $id_account)
        ->first();
        if($check_access->kelola_barang == 1){
            $transaction = Transaction::where('kode_transaksi', $id)->get();
            $jml_barang = count($transaction);
            $jumlah_data = 0;
        	for($i = 0; $i < $jml_barang; $i++){
                $product_status = Product::where('kode_barang', $transaction[$i]->kode_barang)
                ->first();
                if($product_status->stok == 0){
                    $product_status->keterangan = 'Tersedia';
                    $product_status->save();
                }

                $supply = new Supply;
                $supply->kode_barang = $transaction[$i]->kode_barang;
                $product = Product::where('kode_barang', $transaction[$i]->kode_barang)
                ->first();
                $supply->nama_barang = $product->nama_barang;
                $supply->jumlah = $transaction[$i]->jumlah;
                $supply->harga_beli = $product->harga;
                $supply->id_pemasok = Auth::id();
                $supply->pemasok = Auth::user()->nama;
                $supply->save();
                $jumlah_data += 1;
        	}

            $activity = new Activity;
            $activity->id_user = Auth::id();
            $activity->user = Auth::user()->nama;
            $activity->nama_kegiatan = 'retur';
            $activity->jumlah = $jumlah_data;
            $activity->save();

            Transaction::where('kode_transaksi', $id)->delete();
            Session::flash('delete_success', 'Barang berhasil diretur');

            return back();
        }else{
            return back();
        }
    }
}
