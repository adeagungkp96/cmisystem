<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplyBahanBaku extends Model
{
   // Initialize
    protected $fillable = [
        'kode_bahan', 'nama_bahan', 'satuan', 'vendor', 'jumlah', 'harga_beli', 'id_pemasok', 'pemasok',
    ];
}