<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriggerSupplyBahanBaku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE TRIGGER tg_pasok_bahan AFTER INSERT ON supply_bahan_bakus FOR EACH ROW
            BEGIN
                UPDATE bahan_bakus SET stok = stok + NEW.jumlah WHERE kode_bahan = NEW.kode_bahan;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER tg_pasok_bahan');
    }
}
