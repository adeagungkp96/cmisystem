$(document).ready(function(){
  $('input[name=search]').on('keyup', function(){
    var searchTerm = $(this).val().toLowerCase();
    $("tbody tr").each(function(){
      var lineStr = $(this).text().toLowerCase();
      if(lineStr.indexOf(searchTerm) == -1){
        $(this).hide();
      }else{
        $(this).show();
      }
    });
  });
});

$(function() {
  $("form[name='update_form']").validate({
    rules: {
      nama: "required",
      email: {
      	required: true,
      	email: true
      },
      nama_toko: "required",
      telp: "required",
      alamat: "required"
    },
    messages: {
      nama: "Nama tidak boleh kosong",
      email: "Email tidak boleh kosong",
      nama_toko: "Nama Toko tidak boleh kosong",
      telp: "No. Telp tidak boleh kosong",
      alamat: "Alamat tidak boleh kosong"
    },
    errorPlacement: function(error, element) {
        var name = element.attr("name");
        $("#" + name + "_error").html(error);
        $("#" + name + "_error").children().addClass('col-form-label');
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

var validator = $("form[name='update_form']").validate({
    rules: {
      nama: "required",
      email: {
      	required: true,
      	email: true
      },
      nama_toko: "required",
      telp: "required",
      alamat: "required"
    },
    messages: {
      nama: "Nama tidak boleh kosong",
      email: "Email tidak boleh kosong",
      nama_toko: "Nama Toko tidak boleh kosong",
      telp: "No. Telp tidak boleh kosong",
      alamat: "Alamat tidak boleh kosong"
    },
    errorPlacement: function(error, element) {
        var name = element.attr("name");
        $("#" + name + "_error").html(error);
        $("#" + name + "_error").children().addClass('col-form-label');
    },
    submitHandler: function(form) {
      form.submit();
    }
  });

$('.dropdown-search').on('hide.bs.dropdown', function () {
  $('tbody tr').show();
  $('input[name=search]').val('');
});