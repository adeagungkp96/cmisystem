$(function() {
  $("form[name='create_dist_form']").validate({
    rules: {
      nama: "required",
      email: {
      	required: true,
      	email: true
      },
      nama_toko: "required",
      telp: "required",
      alamat: "required"
    },
    messages: {
      nama: "Nama tidak boleh kosong",
      email: "Email tidak boleh kosong",
      nama_toko: "Nama Toko tidak boleh kosong",
      telp: "No. Telp tidak boleh kosong",
      alamat: "Alamat tidak boleh kosong"
    },
    errorPlacement: function(error, element) {
        var name = element.attr("name");
        $("#" + name + "_error").html(error);
        $("#" + name + "_error").children().addClass('col-form-label');
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});