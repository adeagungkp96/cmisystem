<!doctype html>
<title>Site Maintenance</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<style>
  html, body { padding: 0; margin: 0; width: 100%; height: 100%; }
  * {box-sizing: border-box;}
  body { text-align: center; padding: 0; background: #d6433b; color: #fff; font-family: Open Sans; }
  h1 { font-size: 50px; font-weight: 100; text-align: center;}
  body { font-family: Open Sans; font-weight: 100; font-size: 20px; color: #fff; text-align: center; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -ms-flex-align: center; align-items: center;}
  article { display: block; width: 700px; padding: 50px; margin: 0 auto; }
  a { color: #fff; font-weight: bold;}
  a:hover { text-decoration: none; }
</style>

<article>
    <img width="100" src="https://lh3.googleusercontent.com/p/AF1QipOYf3S48pHackcV-_DH-iHFolGGUsv7vxJl301z=s406-no">
    <h1>We&rsquo;ll be back soon!</h1>
    <div style="margin-bottom: 100px">
        <p>Maaf untuk ketidaknyamanannya. Kami sedang melakukan beberapa pemeliharaan saat ini. Jika perlu, Anda bisa menghubungi admin kami untuk informasi, pembelian di link berikut <a href="https://api.whatsapp.com/send?phone=6281320980076" target="_blank">Admin CMI</a> kami akan kembali, Segera!</p>
        <p>&mdash; CMI Team</p>
    </div>
    <a href="https://manufactur.capmawarindonesia.com/login">POS MANUFACTUR</a>
    &nbsp;|&nbsp;
    <a href="https://manufactur.capmawarindonesia.com/dashboard">DASHBOARD ADMIN</a>
</article>
<script>
    

    window.addEventListener('DOMContentLoaded', () => {
    console.log(window.location.origin);
    if(window.location.origin === "https://manufactur.capmawarindonesia.com") {
        window.location.href = window.location.origin+"/login";
    }
    })
</script>