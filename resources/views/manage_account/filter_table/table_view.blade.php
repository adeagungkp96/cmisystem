@foreach($users as $user)
<tr>
  <td>
  	<img src="{{ asset('pictures/' . $user->foto) }}">
  	<span class="ml-2">{{ $user->nama }}</span>
  </td>
  <td>{{ $user->email }}</td>
  <td>
    @if($user->role == 'admin')
    <span class="btn admin-span">{{ $user->role }}</span>
    @else
    <span class="btn kasir-span">{{ $user->role }}</span>
    @endif
  </td>
  <td>
  	<button type="button" class="btn btn-edit btn-icons btn-rounded btn-secondary" data-toggle="modal" data-target="#editModal" data-edit="{{ $user->id }}">
      <i class="mdi mdi-pencil"></i>
    </button>
    <button type="button" data-delete="{{ $user->id }}" class="btn btn-icons btn-rounded btn-secondary ml-1 btn-delete">
        <i class="mdi mdi-close"></i>
    </button>
  </td>
</tr>
@endforeach