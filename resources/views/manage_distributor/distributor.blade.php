@extends('templates/main')
@section('css')
<link rel="stylesheet" href="{{ asset('css/manage_account/account/style.css') }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
@endsection
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header d-flex justify-content-between align-items-center">
      <h4 class="page-title">Daftar Distributor</h4>
      <div class="d-flex justify-content-start">
      	<div class="dropdown">
	        <button class="btn btn-icons btn-inverse-primary btn-filter shadow-sm" type="button" id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          <i class="mdi mdi-filter-variant"></i>
	        </button>
	        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1">
	          <h6 class="dropdown-header">Urut Berdasarkan :</h6>
	          <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item filter-btn" data-filter="no_kontrak">No. Kontrak</a>
	          <a href="#" class="dropdown-item filter-btn" data-filter="nama">Nama</a>
            <a href="#" class="dropdown-item filter-btn" data-filter="email">Email</a>
            <a href="#" class="dropdown-item filter-btn" data-filter="nama_toko">Nama Toko</a>
	        </div>
	      </div>
        <div class="dropdown dropdown-search">
          <button class="btn btn-icons btn-inverse-primary btn-filter shadow-sm ml-2" type="button" id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="mdi mdi-magnify"></i>
          </button>
          <div class="dropdown-menu search-dropdown" aria-labelledby="dropdownMenuIconButton1">
            <div class="row">
              <div class="col-11">
                <input type="text" class="form-control" name="search" placeholder="Cari akun">
              </div>
            </div>
          </div>
        </div>
	      <a href="{{ url('/distributor/new') }}" class="btn btn-icons btn-inverse-primary btn-new ml-2">
	      	<i class="mdi mdi-plus"></i>
	      </a>
      </div>
    </div>
  </div>
</div>
<div class="row modal-group">
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ url('/distributor/update') }}" method="post" enctype="multipart/form-data" name="update_form">
          <div class="modal-header">
            <h5 class="modal-title" id="editModalLabel">Edit Akun</h5>
            <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              @csrf
              <div class="row">
                <div class="col-12" hidden="">
                  <input type="text" name="id">
                </div>
              </div>
              <div class="form-group row mt-4">
                <label class="col-3 col-form-label font-weight-bold">Nama</label>
                <div class="col-9">
                  <input type="text" class="form-control" name="nama">
                </div>
                <div class="col-9 offset-3 error-notice" id="nama_error"></div>
              </div>
              <div class="form-group row">
                <label class="col-3 col-form-label font-weight-bold">Email</label>
                <div class="col-9">
                  <input type="email" class="form-control" name="email">
                </div>
                <div class="col-9 offset-3 error-notice" id="email_error"></div>
              </div>
              <div class="form-group row">
                <label class="col-3 col-form-label font-weight-bold">Nama Toko</label>
                <div class="col-9">
                  <input type="text" class="form-control" name="nama_toko">
                </div>
                <div class="col-9 offset-3 error-notice" id="nama_toko_error"></div>
              </div>
              <div class="form-group row">
                <label class="col-3 col-form-label font-weight-bold">Tanggal Join</label>
                <div class="col-9">
                  <input type="text" class="form-control" name="tanggal_join" id="datepicker">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-3 col-form-label font-weight-bold">Telp</label>
                <div class="col-9">
                  <input type="tel" class="form-control" name="telp">
                </div>
                <div class="col-9 offset-3 error-notice" id="telp_error"></div>
              </div>
              <div class="form-group row">
                <label class="col-3 col-form-label font-weight-bold">Alamat</label>
                <div class="col-9">
                  <textarea type="text" class="form-control" name="alamat"></textarea>
                </div>
                <div class="col-9 offset-3 error-notice" id="alamat_error"></div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-update"><i class="mdi mdi-content-save"></i> Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card card-noborder b-radius">
      <div class="card-body">
        <div class="row">
        	<div class="col-12 table-responsive">
        		<table class="table table-custom">
              <thead>
                <tr>
                  <th>No. Kontrak</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Toko</th>
                  <th>Telp</th>
                  <th>Alamat</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              	@foreach($users as $user)
                <tr>
                  <td>{{ $user->no_kontrak }}</td>
                  <td>{{ $user->nama }}</td>
                  <td><a href="mailto:{{ $user->email }}" style="cursor: pointer; margin-right: 3px"><i class="mdi mdi-email-outline" style="font-size: 20px;vertical-align: -3px;"></i></a>{{ $user->email }}</td>
                  <td>{{ $user->nama_toko }}</td>
                  <td><a href="tel:+{{ $user->telp }}" style="cursor: pointer; margin-right: 3px"><i class="mdi mdi-phone-forward" style="font-size: 20px;vertical-align: -3px;"></i></a><a href="https://wa.me/{{ $user->telp }}" target="_blank" style="cursor: pointer; margin-right: 3px"><i class="mdi mdi-whatsapp" style="font-size: 20px;vertical-align: -3px;"></i></a>{{ $user->telp }}</td>
                  <td>{{ \Illuminate\Support\Str::limit($user->alamat, 20, $end='...') }}</td>
                  <td>
                  	<button type="button" class="btn btn-edit btn-icons btn-rounded btn-secondary" data-toggle="modal" data-target="#editModal" data-edit="{{ $user->id }}">
                        <i class="mdi mdi-pencil"></i>
                    </button>
                    <button type="button" data-delete="{{ $user->id }}" class="btn btn-icons btn-rounded btn-secondary ml-1 btn-delete">
                        <i class="mdi mdi-close"></i>
                    </button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        	</div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<script src="{{ asset('js/manage_distributor/distributor/script.js') }}"></script>
<script type="text/javascript">
  @if ($message = Session::get('create_success'))
    swal(
        "Berhasil!",
        "{{ $message }}",
        "success"
    );
  @endif

  @if ($message = Session::get('update_success'))
    swal(
        "Berhasil!",
        "{{ $message }}",
        "success"
    );
  @endif

  @if ($message = Session::get('delete_success'))
    swal(
        "Berhasil!",
        "{{ $message }}",
        "success"
    );
  @endif

  @if ($message = Session::get('both_error'))
    swal(
    "",
    "{{ $message }}",
    "error"
    );
  @endif

  @if ($message = Session::get('email_error'))
    swal(
    "",
    "{{ $message }}",
    "error"
    );
  @endif

  $(document).on('click', '.filter-btn', function(e){
    e.preventDefault();
    var data_filter = $(this).attr('data-filter');
    $.ajax({
      method: "GET",
      url: "{{ url('/distributor/filter') }}/" + data_filter,
      success:function(data)
      {
        $('tbody').html(data);
      }
    });
  });

  $(document).on('click', '.btn-edit', function(){
    var data_edit = $(this).attr('data-edit');
    $.ajax({
      method: "GET",
      url: "{{ url('/distributor/edit') }}/" + data_edit,
      success:function(response)
      {
        $('.img-edit').attr("src", "{{ asset('pictures') }}/" + response.user.foto);
        $('input[name=id]').val(response.user.id);
        $('input[name=nama]').val(response.user.nama);
        $('input[name=email]').val(response.user.email);
        $('input[name=nama_toko]').val(response.user.nama_toko);
        $('input[name=telp]').val(response.user.telp);
        $('textarea[name=alamat]').val(response.user.alamat);
        var tgl = response.user.no_kontrak.replace('CMIDIST','');
        $('input[name=tanggal_join]').val(tgl[6]+tgl[7]+'/'+tgl[4]+tgl[5]+'/'+tgl[0]+tgl[1]+tgl[2]+tgl[3]);
        validator.resetForm();
      }
    });
  });

  $(document).on('click', '.btn-delete', function(e){
    e.preventDefault();
    var data_delete = $(this).attr('data-delete');
    swal({
      title: "Apa Anda Yakin?",
      text: "Data akun akan terhapus, klik oke untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.open("{{ url('/distributor/delete') }}/" + data_delete, "_self");
      }
    });
  });
  $( function() {
		$( "#datepicker" ).datepicker();
	} );
</script>
@endsection