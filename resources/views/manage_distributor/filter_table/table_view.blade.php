@foreach($users as $user)
<tr>
  <td>{{ $user->no_kontrak }}</td>
  <td>{{ $user->nama }}</td>
  <td><a href="mailto:{{ $user->email }}" style="cursor: pointer; margin-right: 3px"><i class="mdi mdi-email-outline" style="font-size: 20px;vertical-align: -3px;"></i></a>{{ $user->email }}</td>
  <td>{{ $user->nama_toko }}</td>
  <td><a href="tel:+{{ $user->telp }}" style="cursor: pointer; margin-right: 3px"><i class="mdi mdi-phone-forward" style="font-size: 20px;vertical-align: -3px;"></i></a><a href="https://wa.me/{{ $user->telp }}" target="_blank" style="cursor: pointer; margin-right: 3px"><i class="mdi mdi-whatsapp" style="font-size: 20px;vertical-align: -3px;"></i></a>{{ $user->telp }}</td>
  <td>{{ \Illuminate\Support\Str::limit($user->alamat, 20, $end='...') }}</td>
  <td>
  	<button type="button" class="btn btn-edit btn-icons btn-rounded btn-secondary" data-toggle="modal" data-target="#editModal" data-edit="{{ $user->id }}">
      <i class="mdi mdi-pencil"></i>
    </button>
    <button type="button" data-delete="{{ $user->id }}" class="btn btn-icons btn-rounded btn-secondary ml-1 btn-delete">
        <i class="mdi mdi-close"></i>
    </button>
  </td>
</tr>
@endforeach