<!DOCTYPE html>
<html>
<head>
	@if(date('d M, Y', strtotime($tgl_awal)) === date('d M, Y', strtotime($tgl_akhir)))
	<title>Laporan Transaksi ({{ date('d M, Y', strtotime($tgl_awal))}})</title>
	@else 
	<title>Laporan Transaksi ({{ date('d M, Y', strtotime($tgl_awal)) . ' - ' . date('d M, Y', strtotime($tgl_akhir))}})</title>
	@endif
	<style type="text/css">
		html{
			font-family: "Arial", sans-serif;
			margin: 0;
			padding: 0;
		}
		.header{
			background-color: #d3eafc;
			padding: 60px 90px;
		}
		.body{
			padding: 40px 90px;
		}
		/* Text */
		.text-20{
			font-size: 20px;
		}
		.text-18{
			font-size: 18px;
		}
		.text-16{
			font-size: 16px;
		}
		.text-14{
			font-size: 14px;
		}
		.text-12{
			font-size: 12px;
		}
		.text-10{
			font-size: 10px;
		}
		.font-bold{
			font-weight: bold;
		}
		.text-left{
			text-align: left;
		}
		.text-right{
			text-align: right;
		}
		.txt-dark{
			color: #5b5b5b;
		}
		.txt-dark2{
			color: #1d1d1d;
		}
		.txt-blue{
			color: #2a4df1;
		}
		.txt-light{
			color: #acacac;
		}
		.txt-green{
			color: #19d895;
		}
		.txt-red{
			color: #dc3131;
		}
		p{
			margin: 0;
		}

		.d-block{
			display: block;
		}
		.w-100{
			width: 100%;
		}
		.img-td{
			width: 40px;
		}
		.img-td img{
			width: 3rem;
		}
		.mt-2{
			margin-top: 10px;
		}
		.mb-1{
			margin-bottom: 5px;
		}
		.mb-4{
			margin-bottom: 20px;
		}
		.pt-30{
			padding-top: 30px;
		}
		.pt-15{
			padding-top: 15px;
		}
		.pt-5{
			padding-top: 5px;
		}
		.pb-5{
			padding-bottom: 5px;
		}
		table{
			border-collapse: collapse;
		}
		thead tr td{
			border-bottom: 0.5px solid #d9dbe4;
			color: #7e94f6;
			font-size: 12px;
			padding: 5px;
			text-transform: uppercase;
		}
		tbody tr td{
			padding: 7px;
		}
		.border-top-foot{
			border-top: 0.5px solid #d9dbe4;
		}
		.mr-20{
			margin-right: 20px;
		}
		ul{
			padding: 0;
		}
		ul li{
			list-style-type: none;
		}
		.w-300p{
			width: 300px;
		}
	</style>
</head>
<body>
	<div class="header">
		<table class="w-100">
			<tr>
				<td class="img-td text-left"><img src="./images/cmi_bw.png"></td>
				<td class="text-left">
					<p class="text-12 txt-dark d-block" style="font-weight: bold">{{ $market->nama_toko }}</p>
					<p class="text-10 txt-dark d-block">{{ $market->alamat }}</p>
					<p class="text-10 txt-dark d-block">{{ $market->no_telp }}</p>
				</td>
				<td class="text-right">
					<p class="text-20 txt-blue font-bold">LAPORAN TRANSAKSI</p>
				</td>
			</tr>
			<tr>
				<td class="text-left txt-blue text-12 font-bold pt-30" colspan="2">Periode Laporan</td>
				<td class="text-right text-12 txt-dark pt-30">Dicetak {{ \Carbon\Carbon::now()->isoFormat('DD MMM, Y') }}</td>
			</tr>
			<tr>
				@if(date('d M, Y', strtotime($tgl_awal)) === date('d M, Y', strtotime($tgl_akhir)))
				<td class="text-left text-12 txt-dark2" colspan="2">{{ date('d M, Y', strtotime($tgl_awal))}}</td>
				@else 
				<td class="text-left text-12 txt-dark2" colspan="2">{{ date('d M, Y', strtotime($tgl_awal)) . ' - ' . date('d M, Y', strtotime($tgl_akhir))}}</td>
				@endif
				@php
				$nama_users = explode(' ',auth()->user()->nama);
				$nama_user = $nama_users[0];
				@endphp
				<td class="text-right text-12 txt-blue">Oleh {{ $nama_user }}</td>
			</tr>
		</table>
	</div>
	<div class="body">
		<ul>
			@php
			$pemasukan = 0;
			$pemasukan_po = 0;
			$pemasukan_cash = 0;
			$pemasukan_transfer = 0;
			@endphp
			@foreach($dates as $date)
			<li class="text-10 txt-light mt-2">{{ date('d M, Y', strtotime($date)) }}</li>
			@php
			$transactions = \App\Transaction::select('kode_transaksi', 'created_at', 'total', 'pelanggan', 'po', 'diskon', 'add_val', 'value_add_val')
			->whereDate('transactions.created_at', $date)
			->distinct()
			->latest()
			->get();
			@endphp
			<table class="w-100 mb-4">
				<thead>
					<tr>
						<td colspan="5"></td>
					</tr>
				</thead>
				<tbody>
					@foreach($transactions as $transaction)
					<tr>
						@php
                    $products = \App\Transaction::where('kode_transaksi', $transaction->kode_transaksi)
                    ->select('transactions.*')
                    ->get();
                    @endphp
						<td>
							<span class="text-12 txt-dark2 d-block">{{ $transaction->kode_transaksi }}</span>
							<span class="text-10 txt-light d-block">Jam : {{ date('H:i', strtotime($transaction->created_at)) }}</span>
						</td>
						@php
						$pemasukan += $transaction->total;
						if($transaction->po === 1) {
							$pemasukan_po += $transaction->total;
						}elseif($transaction->po === 2) {
							$pemasukan_cash += $transaction->total;
						}else {
							$pemasukan_transfer += $transaction->total;
						}
						$sumjml = \App\Transaction::where('kode_transaksi', $transaction->kode_transaksi)
						->select('transactions.*');
						@endphp
						<td>
							<span class="text-10 txt-light d-block">Pelanggan</span>
							<span class="txt-dark text-12 d-block">{{ $transaction->pelanggan }}</span>
						</td>
						<td>
							<span class="text-10 txt-light d-block">Total Barang</span>
							<span class="txt-dark2 text-12 d-block">{{ $sumjml->sum('jumlah') }} Pcs</span>
						</td>
						<td>
							<span class="text-10 txt-light d-block">Pembayaran</span>
							@if($transaction->po === 1)
							<span class="txt-red text-12 d-block">PO</span>
							@elseif($transaction->po === 2)
							<span class="txt-green text-12 d-block">CASH</span>
							@else
							<span class="txt-green text-12 d-block">TRANSFER</span>
							@endif
						</td>
						@if($transaction->add_val)
						<td>
							<span class="text-10 txt-light d-block">{{$transaction->add_val}}</span>
							<span class="txt-dark2 text-12 d-block">Rp. {{ number_format($transaction->value_add_val,2,',','.') }}</span>
						</td>
						@endif
						<td>
							<span class="text-10 txt-light d-block">Total Transaksi</span>
							<span class="txt-green text-12 d-block">Rp. {{ number_format($transaction->total,2,',','.') }} <small>{{ $transaction->diskon > 0? ' Diskon (' . $transaction->diskon . '%)':'' }}</small></span>
						</td>
					</tr>
					<tr>
						<td colspan="5">
						  <table class="w-100 mb-4" style="background: #f9f9f9;">
							@foreach($products as $product)
							<tr>
							  <td><span class="txt-dark2 text-12 d-block" style="font-size: 10px; padding-left: 10px">{{ $loop->iteration }}</span></td>
							  <td>
								<span class="text-10 txt-light d-block">{{ $product->nama_barang }}</span>
								<span class="txt-green text-12 d-block">{{ $product->kode_barang }}</span>
							  </td>
							  <td>
								<span class="text-10 txt-light d-block">Qty</span>
								<span class="txt-dark text-12 d-block">{{ $product->jumlah }}</span>
							  </td>
							  <td>
								<span class="text-10 txt-light d-block">Harga</span>
								<span class="txt-dark text-12 d-block">Rp. {{ number_format($product->harga,2,',','.') }}</span>
							  </td>
							  <td>
								<span class="text-10 txt-light d-block">Transaksi</span>
								<span class="txt-green text-12 d-block">Rp. {{ number_format($product->total_barang,2,',','.') }}</span>
							  </td>
							</tr>
							<div style="border: 1px solid #eee;"></div>
							@endforeach
						  </table>
						</td>
					  </tr>
					@endforeach
				</tbody>
			</table>
			@endforeach
		</ul>
		<table class="w-100">
			<tfoot>
				<tr>
					<td class="border-top-foot"></td>
				</tr>
				<tr>
					<td class="text-14 pt-15 text-right">
						<span class="mr-20">CASH</span>
						<span class="txt-blue font-bold">Rp. {{ number_format($pemasukan_cash,2,',','.') }}</span>
					</td>
				</tr>
				<tr>
					<td class="text-14 pt-15 text-right">
						<span class="mr-20">TRANSFER</span>
						<span class="txt-blue font-bold">Rp. {{ number_format($pemasukan_transfer,2,',','.') }}</span>
					</td>
				</tr>
				<tr>
					<td class="text-14 pt-15 text-right">
						<span class="mr-20">PO</span>
						<span class="txt-blue font-bold">Rp. {{ number_format($pemasukan_po,2,',','.') }}</span>
					</td>
				</tr>
				<tr>
					<td class="text-14 pt-15 text-right">
						<span class="mr-20"></span>
						<span class="txt-blue font-bold"></span>
					</td>
				</tr>
				<tr>
					<td class="text-14 pt-15 text-right">
						<span class="mr-20">OMZET</span>
						<span class="txt-blue font-bold">Rp. {{ number_format($pemasukan,2,',','.') }}</span>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</body>
</html>