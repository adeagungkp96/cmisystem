<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		*{
			font-family: Arial, sans-serif;
		}

		.center{
			text-align: center;
		}

		.right{
			text-align: right;
		}

		.left{
			text-align: left;
		}

		p{
			font-size: 10px;
		}

		.top-min{
			margin-top: -10px;
		}

		.uppercase{
			text-transform: uppercase;
		}

		.bold{
			font-weight: bold;
		}

		.d-block{
			display: block;
		}

		hr{
			border: 0;
			border-top: 1px solid #000;
		}

		.hr-dash{
			border-style: solid none none none;
		}

		table{
			font-size: 12px;
		}

		table thead tr td{
			padding: 5px;
		}

		.w-100{
			width: 100%;
		}

		.line{
			border: 0;
			border-top: 1px solid #000;
			border-style: solid none none none;
		}

		.body{
			margin-top: -10px;
		}

		.b-p{
			font-size: 12px !important;
		}

		.w-long{
			width: 80px;
		}
	</style>
</head>
<body>
	<div class="header">
		<div style="width: 100%; padding: 0; margin: 0;height: 75px; margin-top: -10px">
			<div style="display: inline-block; width: 7%; padding: 0; margin: 0;">
				<img src="./images/cmi_bw.png" style="width: 100%">
				<span style="height: 4px; display: block"></span>
			</div>
			<div style="display: inline-block; width: 53%; padding: 0; margin: 0; padding-left: 5px;">
				<p class="uppercase bold" style="white-space: nowrap;">{{ $market->nama_toko }}</p>
				<p class="top-min d-block">{{ $market->alamat }}</p>
				<p class="top-min d-block">{{ $market->no_telp }}</p>
			</div>
			<div style="display: inline-block; width: 40%; padding: 0; margin: 0; padding-left: 15px; text-align: right;">
				<p class="uppercase bold" style="font-size: 20px !important; margin-top: 5px; padding-right: 25px">SURAT JALAN</p>
			</div>
		</div>
		<table class="w-100">
			<tr>
				<td class="left" style="width: 43px">From : </td>
				<td class="left">{{ $market->nama_toko }}</td>
				<td class="right">To : </td>
				<td class="right">{{ $transaction->pelanggan }}</td>
			</tr>
			<tr>
				<td class="left" style="width: 43px">Kode : </td>
				<td class="left">{{ $transaction->kode_transaksi }}</td>
				<td class="right">Tanggal : </td>
				<td class="right" style="width: 115px">{{ date('d M, Y', strtotime($transaction->created_at)) . ' ' . date('H:i', strtotime($transaction->created_at)) }}</td>
			</tr>
		</table>
		<hr class="hr-dash">
	</div>
	<div class="body">
		@php
		$counttotalbarang = 0;
		@endphp
		<table class="w-100" style="border-collapse: collapse;margin: 0; padding: 0">
			<thead>
				<tr>
					<td style="font-weight: bold">Nama Barang</td>
					<td style="text-align: right; font-weight: bold">Qty</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaksi)
				@php
				$counttotalbarang += $transaksi->jumlah;
				@endphp
				<tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td style="text-align: right">{{ $transaksi->jumlah }}&nbsp;&nbsp;</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<hr class="hr-dash">
		<table class="w-100" style="border-collapse: collapse;margin-bottom: 7px">
			<tr>
				<td class="left" style="font-weight: bold;padding: 0; margin: 0">Total Barang</td>
				<td class="right" style="font-weight: bold;padding: 0; margin: 0">{{ $counttotalbarang }}</td>
			</tr>
		</table>
		
		<table class="w-100" style="border-collapse: collapse;border: 1px solid;">
			<tr>
				<td class="left" style="border-right: 1px solid; text-align: center; width: 33%;padding: 0; margin: 0; padding-top: 5px">Diterima Oleh</td>
				<td class="right" style="border-right: 1px solid; text-align: center; width: 33%;padding: 0; margin: 0; padding-top: 5px">Mengetahui</td>
				<td class="right" style="text-align: center; width: 33%;padding: 0; margin: 0; padding-top: 5px">Pengirim</td>
			</tr>
			<tr>
				<td class="left" style="border-right: 1px solid; height: 30px;padding: 0; margin: 0"></td>
				<td class="right" style="border-right: 1px solid; height: 30px;padding: 0; margin: 0"></td>
				<td class="right" style="height: 30px;padding: 0; margin: 0"></td>
			</tr>
			<tr>
				<td class="left" style="border-right: 1px solid; height: 30px;padding: 0; margin: 0"></td>
				<td class="right" style="border-right: 1px solid; height: 30px;padding: 0; margin: 0"></td>
				<td class="right" style="height: 30px;padding: 0; margin: 0"></td>
			</tr>
		</table>
	</div>
	<div class="footer">
		<p style="font-size: 10px !important">*Tulis nama dan tanda tangan</p>
	</div>

	<table style="border-collapse: collapse;margin-top: 30px;width: 100%; margin-bottom: 5px; border-bottom: 1px solid">
		<tr>
			<td><b>Rekening Perusahan</b></td>
		</tr>
		<tr>
			<td style="width: 42%">NAMA BANK:</td>
			<td style="width: 33%">NOMOR REKENING:</td>
			<td style="width: 25%">NAMA REKENING:</td>
		</tr>
		<tr>
			<td style="width: 42%; padding-bottom: 9px">BANK CENTRAL ASIA</td>
			<td style="width: 33%; padding-bottom: 9px">8692090593</td>
			<td style="width: 25%; padding-bottom: 9px">PT. CAP MAWAR INDONESIA</td>
			
		</tr>
	</table>
	<table style="border-collapse: collapse;width: 100%; font-weight: bold;margin-top: 9px">
		<tr>
			<td style="font-size: 9.5px; text-align: center">PARFUM LAUNDRY
			| HAND SANITIZER
			| SABUN CUCI TANGAN
			| SABUN CUCI PIRING
			| DETERJEN
			| SOFTENER
			| KARBOL SEREH
			| PEMBERSIH KERAK</td>
		</tr>
	</table>
	<div style="font-size: 14px; width: 100%;text-align: center; margin-top: 3px; font-weight: bold">HOME CARE - SELF CARE</div>
	<div style="font-size: 9px; width: 100%;text-align: center; margin-top: 1px">www.capmawarindonesia.com</div>
</body>
</html>