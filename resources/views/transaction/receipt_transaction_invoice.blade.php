<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		*{
			font-family: Arial, sans-serif;
		}

		.center{
			text-align: center;
		}

		.right{
			text-align: right;
		}

		.left{
			text-align: left;
		}

		p{
			font-size: 10px;
		}

		.top-min{
			margin-top: -10px;
		}

		.uppercase{
			text-transform: uppercase;
		}

		.bold{
			font-weight: bold;
		}

		.d-block{
			display: block;
		}

		hr{
			border: 0;
			border-top: 1px solid #000;
		}

		.hr-dash{
			border-style: solid none none none;
		}

		table{
			font-size: 12px;
		}

		table thead tr td{
			padding: 5px;
		}

		.w-100{
			width: 100%;
		}

		.line{
			border: 0;
			border-top: 1px solid #000;
			border-style: solid none none none;
		}

		.body{
			margin-top: -10px;
		}

		.b-p{
			font-size: 12px !important;
		}

		.w-long{
			width: 80px;
		}
	</style>
</head>
<body>
	<div class="header">
		<div style="width: 100%; padding: 0; margin: 0;height: 75px; margin-top: -10px">
			<div style="display: inline-block; width: 7%; padding: 0; margin: 0;">
				<img src="./images/cmi_bw.png" style="width: 100%">
				<span style="height: 4px; display: block"></span>
			</div>
			<div style="display: inline-block; width: 53%; padding: 0; margin: 0; padding-left: 5px;">
				<p class="uppercase bold" style="white-space: nowrap;">{{ $market->nama_toko }}</p>
				<p class="top-min d-block">{{ $market->alamat }}</p>
				<p class="top-min d-block">{{ $market->no_telp }}</p>
			</div>
			<div style="display: inline-block; width: 40%; padding: 0; margin: 0; padding-left: 15px; text-align: right;">
				<p class="uppercase bold" style="font-size: 20px !important; margin-top: 5px; padding-right: 25px">INVOICE</p>
			</div>
		</div>
		<table class="w-100">
			<tr>
				<td class="left" style="width: 43px">Kasir : </td>
				<td class="left">{{ $transaction->kasir }}</td>
				<td class="right">Cust : </td>
				<td class="right">{{ $transaction->pelanggan }}</td>
			</tr>
			<tr>
				<td class="left" style="width: 43px">Kode : </td>
				<td class="left">{{ $transaction->kode_transaksi }}</td>
				<td class="right">Tanggal : </td>
				<td class="right" style="width: 115px">{{ date('d M, Y', strtotime($transaction->created_at)) . ' ' . date('H:i', strtotime($transaction->created_at)) }}</td>
			</tr>
		</table>
		<hr class="hr-dash">
	</div>
	<div class="body">
		@php
		$counttotalbarang = 0;
		@endphp
		<table class="w-100" style="border-collapse: collapse;margin: 0; padding: 0">
			<thead>
				{{-- <tr>
					<td>Nama Barang</td>
					<td style="text-align: right">Qty</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr> --}}
				<tr>
					<td style="font-weight: bold">Nama Barang</td>
					<td style="font-weight: bold">Qty</td>
					<td style="text-align: right; font-weight: bold">Harga</td>
					<td style="text-align: right; font-weight: bold">Jumlah</td>
				</tr>
				<tr>
					<td colspan="4" class="line"></td>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $transaksi)
				@php
				$counttotalbarang += $transaksi->jumlah;
				@endphp
				{{-- <tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td style="text-align: right">{{ $transaksi->jumlah }}&nbsp;&nbsp;</td>
				</tr> --}}
				<tr>
					<td style="padding-left: 5px">{{ $transaksi->nama_barang }}</td>
					<td>{{ $transaksi->jumlah }} pcs</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->harga,2,',','.') }}</td>
					<td style="text-align: right; padding-right: 5px">{{ number_format($transaksi->total_barang,2,',','.') }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<hr class="hr-dash">
		<table class="w-100" style="border-collapse: collapse;margin-bottom: 7px">
			<tr>
				<td class="left">Subtotal (Jumlah : {{ $counttotalbarang }})</td>
				<td class="right">{{ number_format($transaction->subtotal,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Diskon ({{ $transaction->diskon }}%)</td>
				<td class="right">{{ number_format($diskon?'-'.$diskon:$diskon,2,',','.') }}</td>
			</tr>
			@if($transaction->add_val)
			<tr>
				<td class="left">{{ $transaction->add_val }}</td>
				<td class="right">{{ number_format($transaction->value_add_val,2,',','.') }}</td>
			</tr>
			@endif
			<tr>
				<td class="left" style="font-weight: bold;padding: 0; margin: 0">Total</td>
				<td class="right" style="font-weight: bold;padding: 0; margin: 0">{{ number_format($transaction->total,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left"></td>
				<td class="right"></td>
			</tr>
			<tr>
				<td class="left" style="font-weight: bold;padding: 0; margin: 0">Pembayaran<style>
					.clsd {
					  padding-left: 4px !important;
					  padding-right: 2px !important;
					  padding-bottom: 1px !important;
					  border: 1px solid #495ff1;
					  border-left: 1px solid #495ff1 !important;
					  font-size: 12px;
					  color: #495ff1;
					  font-weight: bold;
					}
					.clsd.po {
					  border-color: #eb5454;
					  color: #eb5454;
					}
				  </style>
				  @if($transaction->po === 1)
				  <span class="clsd po">PO</span>
				  @elseif($transaction->po === 2)
				  <span class="clsd">CASH</span>
				  @else
				  <span class="clsd">TRANSFER</span>
				  @endif</td>
				<td class="right" style="font-weight: bold;padding: 0; margin: 0">{{ number_format($transaction->bayar,2,',','.') }}</td>
			</tr>
			<tr>
				<td class="left">Kembali</td>
				<td class="right">{{ number_format($transaction->kembali,2,',','.') }}</td>
			</tr>
		</table>
		<table style="border-collapse: collapse;margin-top: 30px;width: 100%; margin-bottom: 5px; border-bottom: 1px solid">
			<tr>
				<td><b>Rekening Perusahan</b></td>
			</tr>
			<tr>
				<td style="width: 42%">NAMA BANK:</td>
				<td style="width: 33%">NOMOR REKENING:</td>
				<td style="width: 25%">NAMA REKENING:</td>
			</tr>
			<tr>
				<td style="width: 42%; padding-bottom: 9px">BANK CENTRAL ASIA</td>
				<td style="width: 33%; padding-bottom: 9px">8692090593</td>
				<td style="width: 25%; padding-bottom: 9px">PT. CAP MAWAR INDONESIA</td>
				
			</tr>
		</table>
		<table style="border-collapse: collapse;width: 100%; font-weight: bold;margin-top: 9px">
			<tr>
				<td style="font-size: 9.5px; text-align: center">PARFUM LAUNDRY
				| HAND SANITIZER
				| SABUN CUCI TANGAN
				| SABUN CUCI PIRING
				| DETERJEN
				| SOFTENER
				| KARBOL SEREH
				| PEMBERSIH KERAK</td>
			</tr>
		</table>
		<div style="font-size: 14px; width: 100%;text-align: center; margin-top: 3px; font-weight: bold">HOME CARE - SELF CARE</div>
		<div style="font-size: 9px; width: 100%;text-align: center; margin-top: 1px">www.capmawarindonesia.com</div>
	</div>
	{{-- <div class="footer">
		<p style="font-size: 8px !important">*Tulis nama dan tanda tangan</p>
	</div> --}}
</body>
</html>