<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return redirect('/dashboard');
	// return redirect('/under-maintenance');
});
Route::get('/product-count', 'ProductManageController@countProduct');
Route::get('/bahanbaku-count', 'BahanBakuManageController@countBahanBaku');

Route::get('/under-maintenance', 'AuthManageController@viewMaintenance');
Route::get('/login', 'AuthManageController@viewLogin')->name('login');
Route::post('/verify_login', 'AuthManageController@verifyLogin');
Route::post('/first_account', 'UserManageController@firstAccount');

Route::group(['middleware' => ['auth', 'checkRole:admin,kasir']], function(){
	Route::get('/logout', 'AuthManageController@logoutProcess');
	Route::get('/dashboard', 'ViewManageController@viewDashboard');
	Route::get('/dashboard/chart/{filter}', 'ViewManageController@filterChartDashboard');
	Route::post('/market/update', 'ViewManageController@updateMarket');
	// ------------------------- Fitur Cari -------------------------
	Route::get('/search/{word}', 'SearchManageController@searchPage');
	// ------------------------- Profil -------------------------
	Route::get('/profile', 'ProfileManageController@viewProfile');
	Route::post('/profile/update/data', 'ProfileManageController@changeData');
	Route::post('/profile/update/password', 'ProfileManageController@changePassword');
	Route::post('/profile/update/picture', 'ProfileManageController@changePicture');
	// ------------------------- Kelola Akun -------------------------
	// > Akun
	Route::get('/account', 'UserManageController@viewAccount');
	Route::get('/account/new', 'UserManageController@viewNewAccount');
	Route::post('/account/create', 'UserManageController@createAccount');
	Route::get('/account/edit/{id}', 'UserManageController@editAccount');
	Route::post('/account/update', 'UserManageController@updateAccount');
	Route::get('/account/delete/{id}', 'UserManageController@deleteAccount');
	Route::get('/account/filter/{id}', 'UserManageController@filterTable');
	// > Distributor
	Route::get('/distributor', 'DistributorManageController@viewDistributor');
	Route::get('/distributor/new', 'DistributorManageController@viewNewDistributor');
	Route::post('/distributor/create', 'DistributorManageController@createDistributor');
	Route::post('/distributor/ajax/create', 'DistributorManageController@createDistributorGet');
	Route::get('/distributor/edit/{id}', 'DistributorManageController@editDistributor');
	Route::post('/distributor/update', 'DistributorManageController@updateDistributor');
	Route::get('/distributor/delete/{id}', 'DistributorManageController@deleteDistributor');
	Route::get('/distributor/filter/{id}', 'DistributorManageController@filterTable');
	// > Akses
	Route::get('/access', 'AccessManageController@viewAccess');
	Route::get('/access/change/{user}/{access}', 'AccessManageController@changeAccess');
	Route::get('/access/check/{user}', 'AccessManageController@checkAccess');
	Route::get('/access/sidebar', 'AccessManageController@sidebarRefresh');
	// ------------------------- Kelola Barang -------------------------
	// > Barang
	Route::get('/product', 'ProductManageController@viewProduct');
	Route::get('/product/new', 'ProductManageController@viewNewProduct');
	Route::post('/product/create', 'ProductManageController@createProduct');
	Route::post('/product/import', 'ProductManageController@importProduct');
	Route::get('/product/edit/{id}', 'ProductManageController@editProduct');
	Route::post('/product/update', 'ProductManageController@updateProduct');
	Route::get('/product/delete/{id}', 'ProductManageController@deleteProduct');
	Route::get('/product/filter/{id}', 'ProductManageController@filterTable');
	Route::post('/product/stock/export', 'ProductManageController@exportProductStockExport');
	Route::post('/product/stock/exportexcel', 'ProductManageController@exportProductStockExportExcel');
	// > Bahan Baku
	Route::get('/bahan-baku', 'BahanBakuManageController@viewBahanBaku');
	Route::get('/bahan-baku/new', 'BahanBakuManageController@viewNewBahanBaku');
	Route::post('/bahan-baku/create', 'BahanBakuManageController@createBahanBaku');
	Route::post('/bahan-baku/import', 'BahanBakuManageController@importBahanBaku');
	Route::get('/bahan-baku/edit/{id}', 'BahanBakuManageController@editBahanBaku');
	Route::post('/bahan-baku/update', 'BahanBakuManageController@updateBahanBaku');
	Route::get('/bahan-baku/delete/{id}', 'BahanBakuManageController@deleteBahanBaku');
	Route::get('/bahan-baku/filter/{id}', 'BahanBakuManageController@filterTable');
	Route::post('/bahan-baku/stock/export', 'BahanBakuManageController@exportBahanBakuStockExport');
	Route::post('/bahan-baku/stock/exportexcel', 'BahanBakuManageController@exportBahanBakuStockExportExcel');
	// > Pasok
	Route::get('/supply/system/{id}', 'SupplyManageController@supplySystem');
	Route::get('/supply', 'SupplyManageController@viewSupply');
	Route::get('/supply/new', 'SupplyManageController@viewNewSupply');
	Route::get('/supply/check/{id}', 'SupplyManageController@checkSupplyCheck');
	Route::get('/supply/data/{id}', 'SupplyManageController@checkSupplyData');
	Route::post('/supply/create', 'SupplyManageController@createSupply');
	Route::post('/supply/import', 'SupplyManageController@importSupply');
	Route::get('/supply/statistics', 'SupplyManageController@statisticsSupply');
	Route::get('/supply/statistics/product/{id}', 'SupplyManageController@statisticsProduct');
	Route::get('/supply/statistics/users/{id}', 'SupplyManageController@statisticsUsers');
	Route::get('/supply/statistics/table/{id}', 'SupplyManageController@statisticsTable');
	Route::post('/supply/statistics/export', 'SupplyManageController@exportSupply');
	// > Pasok Bahan
	Route::get('/supplybahanbaku/system/{id}', 'SupplyBahanBakuManageController@supplySystem');
	Route::get('/supplybahanbaku', 'SupplyBahanBakuManageController@viewSupply');
	Route::get('/supplybahanbaku/new', 'SupplyBahanBakuManageController@viewNewSupply');
	Route::get('/supplybahanbaku/check/{id}', 'SupplyBahanBakuManageController@checkSupplyCheck');
	Route::get('/supplybahanbaku/data/{id}', 'SupplyBahanBakuManageController@checkSupplyData');
	Route::post('/supplybahanbaku/create', 'SupplyBahanBakuManageController@createSupply');
	Route::post('/supplybahanbaku/import', 'SupplyBahanBakuManageController@importSupply');
	Route::get('/supplybahanbaku/statistics', 'SupplyBahanBakuManageController@statisticsSupply');
	Route::get('/supplybahanbaku/statistics/product/{id}', 'SupplyBahanBakuManageController@statisticsProduct');
	Route::get('/supplybahanbaku/statistics/users/{id}', 'SupplyBahanBakuManageController@statisticsUsers');
	Route::get('/supplybahanbaku/statistics/table/{id}', 'SupplyBahanBakuManageController@statisticsTable');
	Route::post('/supplybahanbaku/statistics/export', 'SupplyBahanBakuManageController@exportSupply');
	// ------------------------- Transaksi -------------------------
	Route::get('/transaction', 'TransactionManageController@viewTransaction');
	Route::get('/transaction/product/{id}', 'TransactionManageController@transactionProduct');
	Route::get('/transaction/refresh-stock', 'TransactionManageController@refreshStock');
	Route::get('/transaction/product/check/{id}', 'TransactionManageController@transactionProductCheck');
	Route::post('/transaction/process', 'TransactionManageController@transactionProcess');
	Route::get('/transaction/receipt/{id}', 'TransactionManageController@receiptTransaction');
	Route::get('/transaction/receipt-full/{id}', 'TransactionManageController@receiptTransactionFull');
	Route::get('/transaction/receipt-do/{id}', 'TransactionManageController@receiptTransactionDo');
	Route::get('/transaction/pocash/{id}', 'TransactionManageController@changepocashTransaction');
	Route::get('/transaction/potransfer/{id}', 'TransactionManageController@changepotransferTransaction');
	Route::get('/transaction/retur/{id}', 'TransactionManageController@returTransaction');
	// ------------------------- Kelola Laporan -------------------------
	Route::get('/report/transaction', 'ReportManageController@reportTransaction');
	Route::get('/report/transaction/filter/income', 'ReportManageController@filterTransactionIncome');
	Route::get('/report/transaction/filter', 'ReportManageController@filterTransaction');
	Route::get('/report/transaction/chart/{id}', 'ReportManageController@chartTransaction');
	Route::get('/report/transaction/total/{id}', 'ReportManageController@totTransaction');
	Route::get('/report/laba/chart/{id}', 'ReportManageController@chartTransactionLaba');
	Route::get('/report/laba/total/{id}', 'ReportManageController@totTransactionLaba');
	Route::post('/report/transaction/export', 'ReportManageController@exportTransaction');
	Route::get('/report/income', 'ReportManageController@reportTransactionIncome');
	Route::post('/report/income/export', 'ReportManageController@exportTransactionIncome');
	Route::get('/report/workers', 'ReportManageController@reportWorker');
	Route::get('/report/workers/filter/{id}', 'ReportManageController@filterWorker');
	Route::get('/report/workers/detail/{id}', 'ReportManageController@detailWorker');
	Route::post('/report/workers/export/{id}', 'ReportManageController@exportWorker');
});

// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');